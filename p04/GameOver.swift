//
//  GameOver.swift
//  p04
//
//  Created by Ahmet on 3/13/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//
import SpriteKit

class GameOver: SKScene{
    override init(size: CGSize){
        super.init(size: size)
        
        self.backgroundColor = .darkGray
        // initialize game with game over label and play again button
        
        let game_over_label = SKLabelNode()
        game_over_label.text = "Game Over"
        game_over_label.fontSize = 48
        game_over_label.fontColor = .white
        game_over_label.position = CGPoint(x: self.size.width/2, y: self.size.height/2 + 48)
        self.addChild(game_over_label)
        
        let replay_button = SKLabelNode()
        replay_button.text = "Play Again"
        replay_button.fontColor = .white
        replay_button.position = CGPoint(x: self.size.width/2, y: self.size.height/2 - 48)
        replay_button.name = "replay"
        self.addChild(replay_button)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            let node = self.nodes(at: location).first
            // if user touch to replay button
            if node?.name == "replay"{
                // present again game scene
                let reveal: SKTransition = SKTransition.fade(withDuration: 0.5)
                let scene = GameScene(size: self.view!.bounds.size)
                scene.scaleMode = .aspectFill
                self.view?.presentScene(scene, transition: reveal)
            }
                
        }
    }
    
    func set_score(score: Int){
        // set score of the user
        let game_over_label = SKLabelNode()
        game_over_label.text = "Score: \(score)"
        game_over_label.fontSize = 24
        game_over_label.fontColor = .white
        game_over_label.position = CGPoint(x: self.size.width/2, y: self.size.height/2 )
        self.addChild(game_over_label)
    }
    
}
