//
//  GameScene.swift
//  p04
//
//  Created by Ahmet on 3/12/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//

import SpriteKit
import Foundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    
      
    var monkey = SKSpriteNode()
    
    var texture_atlas = SKTextureAtlas()
    var texture_array = [SKTexture]()
    
    var move_up = SKAction()
    var move_down = SKAction()
    
    var background_velocity: CGFloat = 3.0
    var level: CGFloat = 1.0
    let sprite = SKSpriteNode(imageNamed: "monkey_1.png")
    
    var last_banana_added: TimeInterval = 0
    var recent_position = 0
    
    var banana_count = 0
    
    let monkey_category = 0x1 << 1
    let banana_category = 0x1 << 2
    
    var score = 0
    
    override func didMove(to view: SKView) {
        self.backgroundColor = .white
        self.add_background()
        self.add_monkey()
        self.physicsWorld.gravity = CGVector(dx:0, dy: 0)
        self.physicsWorld.contactDelegate = self
        
    }
    
    func add_monkey(){
        
        self.addChild(sprite)
        
        let position = CGPoint(x: 100 , y: 75)
        sprite.position = position
        sprite.size = CGSize(width: 85, height: 85)

        // animated monkey based on 16 pictures
        let atlas = SKTextureAtlas(named: "monkey")
        let m1 = atlas.textureNamed("monkey_1.png")
        let m2 = atlas.textureNamed("monkey_2.png")
        let m3 = atlas.textureNamed("monkey_3.png")
        let m4 = atlas.textureNamed("monkey_4.png")
        let m5 = atlas.textureNamed("monkey_5.png")
        let m6 = atlas.textureNamed("monkey_6.png")
        let m7 = atlas.textureNamed("monkey_7.png")
        let m8 = atlas.textureNamed("monkey_8.png")
        let m9 = atlas.textureNamed("monkey_9.png")
        let m10 = atlas.textureNamed("monkey_10.png")
        let m11 = atlas.textureNamed("monkey_11.png")
        let m12 = atlas.textureNamed("monkey_12.png")
        let m13 = atlas.textureNamed("monkey_13.png")
        let m14 = atlas.textureNamed("monkey_14.png")
        let m15 = atlas.textureNamed("monkey_15.png")
        let m16 = atlas.textureNamed("monkey_16.png")
        
        let textures = [m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16]
        // picture frame reduces when level increase
        let meleeAnimation = SKAction.animate(with: textures, timePerFrame: 0.1 / Double(self.level))
        
        // collision between monkey and banana
        sprite.physicsBody = SKPhysicsBody(rectangleOf: sprite.size)
        sprite.physicsBody?.categoryBitMask = UInt32(monkey_category)
        sprite.physicsBody?.isDynamic = true
        sprite.physicsBody?.contactTestBitMask = UInt32(banana_category)
        sprite.physicsBody?.collisionBitMask = 0
        sprite.physicsBody?.affectedByGravity = false
        sprite.name = "monkey"
        
        move_up = SKAction.moveBy(x: 0, y: 20, duration: 0.2)
        move_down = SKAction.moveBy(x: 0, y: -20, duration: 0.2)
        
        sprite.run(SKAction.repeatForever(meleeAnimation))
    }
    
    func add_banana(){
        // when you saw 100 banana game'll end
        if banana_count >= 100{
            // present game over screene
            let game_over_scene = GameOver(size: self.size)
            self.view?.presentScene(game_over_scene, transition: .fade(withDuration: 0.5))
            game_over_scene.set_score(score: score)
        }
        let banana = SKSpriteNode(imageNamed: "banana")
        banana.xScale = 0.5
        banana.yScale = 0.5
        banana.physicsBody = SKPhysicsBody(rectangleOf: banana.frame.size)
        banana.physicsBody?.categoryBitMask = UInt32(banana_category)
        banana.physicsBody?.isDynamic = true
        banana.physicsBody?.contactTestBitMask = UInt32(monkey_category)
        banana.physicsBody?.collisionBitMask = 0
        banana.physicsBody?.affectedByGravity = false
        banana.physicsBody?.usesPreciseCollisionDetection = true
        banana.name = "banana"
        
        // select line from imaginary y lines
        let possible_y: [Int] = [30, 50, 70]
        let randomIndex = Int(arc4random_uniform(UInt32(possible_y.count)))
        let random_y: CGFloat = CGFloat(possible_y[randomIndex])
        banana.position = CGPoint(x: self.frame.size.width + 20, y: random_y)
        self.addChild(banana)
        
        banana_count += 1
        
    }
    
    func add_background(){
        for index in 0..<2{
            let back = SKSpriteNode(imageNamed: "back")
            back.position = CGPoint(x:index * Int(back.size.width), y: 0)
            back.anchorPoint = CGPoint(x: 0, y: 0)
            back.name = "background"
            self.addChild(back)
        }
    }
    
    func move_background(){
        // moves background left node to right when all background is seen
        // speed increases with level
        self.enumerateChildNodes(withName: "background", using: {(node, stop) -> Void in
            if let back = node as? SKSpriteNode{
                back.position = CGPoint(x:  back.position.x - self.background_velocity * self.level, y: back.position.y)
                if back.position.x <= -back.size.width{
                    back.position = CGPoint(x: back.position.x + back.size.width * 2, y: back.position.y)
                }
            }
        })
    }
    func move_banana(){
        // moves banana nodes with background speed
        // speed increases with level
        self.enumerateChildNodes(withName: "banana", using: {(node, stop) -> Void in
            if let banana = node as? SKSpriteNode{
                banana.position = CGPoint(x:  banana.position.x - self.background_velocity * self.level, y: banana.position.y)
                if banana.position.x <= 0{
                    banana.removeFromParent()
                }
            }
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            // if you touch upside of the half of the screen
            if location.y > self.size.height/2{
                // if it's possible to go up
                if sprite.position.y <= 80{
                    // it moves up
                    sprite.run(move_up)
                    recent_position += 1
                }
            } // below of the half of the screen
            else{
                // if it's possible to go down
                if sprite.position.y >= 60{
                    // it goes down
                    sprite.run(move_down)
                    recent_position -= 1
                }
            }
        }
    }
    func didBegin(_ contact: SKPhysicsContact) {
        
        var first_body = SKPhysicsBody()
        var second_body = SKPhysicsBody()
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            first_body = contact.bodyA
            second_body = contact.bodyB
        }else{
            first_body = contact.bodyB
            second_body = contact.bodyA
        }
        //NSLog("\(Int((second_body.node?.position.y)!)), \(recent_position)")
        
        // if banana and monkey contacted
        if (first_body.categoryBitMask & UInt32(monkey_category) != 0 &&
            second_body.categoryBitMask & UInt32(banana_category) != 0){
            let y_pos = round((second_body.node?.position.y)!)
            // check if banana and monkey at same position
            if ((y_pos == 30 && recent_position == -1) ||
                (y_pos == 50 && recent_position == 0) ||
                (y_pos == 70 && recent_position == 1))
                {
                    // remove banana
                    second_body.node?.removeFromParent()
                    score += 1
                
            }
        }

    }
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        self.move_background()
        self.move_banana()
        // when bana and background speeds up
        // it will add banana fast
        if currentTime - self.last_banana_added > 3.0 / Double(self.level){
            self.last_banana_added = currentTime + 1.0 / Double(self.level)
            self.add_banana()
            level += 0.1
            
        }
    }
}
